﻿using UnityEngine;
using Zenject;
using System;

public class ArrowManager : ITickable {


    readonly private Settings _settings;

    readonly private LineRenderer _lineRenderer;

    private bool isRendering = false;


    public ArrowManager(
        Settings settings,
        RenderArrowSignal renderArrowSignal,
        BallShotSignal ballShotSignal,
        LineRenderer lineRenderer)
    {
        _lineRenderer = lineRenderer;
        _settings = settings;
        renderArrowSignal += OnArrowRender;
        ballShotSignal += OnBallShot;
    }

    public void OnArrowRender()
    {
        _lineRenderer.enabled = true;
        isRendering = true;
    }
    public void OnBallShot(Vector2 direction)
    {
        _lineRenderer.enabled = false;
        isRendering = false;
    }

    private void RenderArrow(Vector2 mousePos)
    {
        _lineRenderer.positionCount = 2;
        _lineRenderer.SetPosition(0, _settings.shootingPos);
        _lineRenderer.SetPosition(1, FindEndPoint(mousePos));
    }

    private Vector2 FindEndPoint(Vector2 endPos)
    {
        RaycastHit2D hit;

        hit = Physics2D.Raycast(_settings.shootingPos, endPos - _settings.shootingPos);
        if (hit)
        {
            endPos = hit.point;
        }
        return endPos;
    }

    public void Tick()
    {
        if (isRendering)
        {
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RenderArrow(mousePos);
        }
    }

    [Serializable]
    public class Settings
    {
        public Vector2 shootingPos;
    }
}
