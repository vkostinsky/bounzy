﻿using UnityEngine;
using Zenject;
using System;

public class InputHandler : ITickable
{
    BallShotSignal _ballShotSignal;
    RenderArrowSignal _renderArrowSignal;

    private bool isShootingEnabled = true;

    [Inject]
    public void Construct(
        BallShotSignal ballShotSignal,
        RenderArrowSignal renderArrowSignal,
        RowsMovingFinishedSignal rowsMovingFinishedSignal,
        GameFinishedSignal gameFinishedSignal)
    {
        _ballShotSignal = ballShotSignal;
        _renderArrowSignal = renderArrowSignal;
        ballShotSignal += OnShotFired;
        rowsMovingFinishedSignal += OnReadyToShot;
        gameFinishedSignal += (_ => isShootingEnabled = false);

    }

    public void Tick()
    {
        if (isShootingEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _renderArrowSignal.Fire();
            }
            if (Input.GetMouseButtonUp(0))
            {
                _ballShotSignal.Fire(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            }
        }
    }
    private void OnShotFired(Vector2 direction)
    {
        isShootingEnabled = false;
    }

    private void OnReadyToShot()
    {
        isShootingEnabled = true;
    }
    private void OnGameFinished(bool isWin)
    {
        isShootingEnabled = false;
    }
}
