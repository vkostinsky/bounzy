﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameFinishHandler
{
    private readonly ShowFinishMessageSignal _showFinishMessageSignal;

    public GameFinishHandler(     
           GameFinishedSignal gameFinishedSignal,
           ShowFinishMessageSignal showFinishMessage)
    {
        gameFinishedSignal += HandleFinish;
        _showFinishMessageSignal = showFinishMessage;
    }
    
    //to-do: inject string for messsage
    private void HandleFinish(bool isWin)
    {
        if(isWin)
        {
            _showFinishMessageSignal.Fire("You Win!");
        }
        else
        {
            _showFinishMessageSignal.Fire("You Lost!");
        }
    }
}
