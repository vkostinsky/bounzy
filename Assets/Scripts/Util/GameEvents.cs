﻿using Zenject;
using UnityEngine;

public class RenderArrowSignal : Signal<RenderArrowSignal>
{
}
public class BallShotSignal : Signal<BallShotSignal, Vector2>
{
}
public class BallShotFinishedSignal : Signal<BallShotFinishedSignal>
{
}
public class RowsMovingFinishedSignal : Signal<RowsMovingFinishedSignal>
{
}
public class BrickDestroyedSignal : Signal<BrickDestroyedSignal, Brick>
{
}
public class BallDestroyedSignal : Signal<BallDestroyedSignal>
{
}
public class GameFinishedSignal : Signal<GameFinishedSignal, bool> //flag marks is win condition
{
}
public class ShowFinishMessageSignal : Signal<ShowFinishMessageSignal, string>
{
}
//public class GamePausedSignal : Signal<GamePausedSignal>
//{
//}
//public class GameUnpausedSignal : Signal<GameUnpausedSignal>
//{
//}
//public class ReadyToShotSignal : Signal<ReadyToShotSignal>
//{
//}
//public class ReadyToMoveSignal : Signal<ReadyToMoveSignal>
//{
//}