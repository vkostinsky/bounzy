﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiHandler : MonoBehaviour, IInitializable {

    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _finishMessage;
    [SerializeField] private GameObject _finishPanel;
    private int score = 0;
     
    [Inject]
    public void Construct(
        BrickDestroyedSignal brickDestroyedSignal,
        ShowFinishMessageSignal showFinishMessageSignal)
    {
        brickDestroyedSignal += UpdateScore;
        showFinishMessageSignal += OnGameFinishUiShow;
    }

    public void Initialize()
    {
        UpdateScoreView(0);
    }

    void UpdateScore(Brick brick)
    {
        score += brick.InitialValue;
        UpdateScoreView(score);
    }
    void UpdateScoreView(int score)
    {
        _scoreText.text = "Score: " + score;//$"Score: {score}";
    }
    void OnGameFinishUiShow(string message)
    {
        _finishPanel.SetActive(true);
        _finishMessage.text = message;
    }
    public void OnRetryButtonPressed()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
