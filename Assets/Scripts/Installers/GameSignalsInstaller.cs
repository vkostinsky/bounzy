﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameSignalsInstaller : Installer<GameSignalsInstaller>
{
    public override void InstallBindings()
    {
        Container.DeclareSignal<RenderArrowSignal>();
        Container.DeclareSignal<BallShotSignal>();
        Container.DeclareSignal<BallShotFinishedSignal>();
        Container.DeclareSignal<RowsMovingFinishedSignal>();
        Container.DeclareSignal<BrickDestroyedSignal>();
        Container.DeclareSignal<BallDestroyedSignal>();
        Container.DeclareSignal<GameFinishedSignal>();
        Container.DeclareSignal<ShowFinishMessageSignal>();
    }
}
