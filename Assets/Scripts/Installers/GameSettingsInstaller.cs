﻿using UnityEngine;
using Zenject;
using System;

[CreateAssetMenu(menuName = "Bounzy/Game Settings")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
{

    public BallManager.Settings ball;
    public BrickManager.Settings brick;
    public GameInstaller.Settings gameInstaller;
    public ArrowManager.Settings arrow;

    public override void InstallBindings()
    {
        Container.BindInstance(gameInstaller);
        Container.BindInstance(arrow);
        Container.BindInstance(brick);
        Container.BindInstance(ball);
    }
}
