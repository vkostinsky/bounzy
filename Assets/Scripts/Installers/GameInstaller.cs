﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class GameInstaller : MonoInstaller
{
    [Inject]
    Settings _settings = null;

    public override void InstallBindings()
    {

        InstallBricks();
        InstallBalls();
        InstallMisc();
        GameSignalsInstaller.Install(Container);

    }

    void InstallBricks()
    {
        Container.BindInterfacesAndSelfTo<BrickManager>().AsSingle().NonLazy();

        Container.BindMemoryPool<Brick, Brick.Pool>()
                .WithInitialSize(20)
                .FromComponentInNewPrefab(_settings.brickPrefab)
                .WithGameObjectName("Brick")
                .UnderTransformGroup("Bricks");
    }

    void InstallBalls()
    {
        Container.Bind<BallManager>().AsSingle().NonLazy();

        Container.BindMemoryPool<Ball, Ball.Pool>()
                .WithInitialSize(1)
                .FromComponentInNewPrefab(_settings.ballPrefab)
                .WithGameObjectName("Ball")
                .UnderTransformGroup("Balls");
    }

    void InstallMisc()
    {
        Container.BindInterfacesAndSelfTo<ArrowManager>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<InputHandler>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameFinishHandler>().AsSingle().NonLazy();
    }


    [Serializable]
    public class Settings
    {
        public GameObject ballPrefab;
        public GameObject brickPrefab;
    }
}
