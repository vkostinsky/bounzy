﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using Random = UnityEngine.Random;
public class Brick : MonoBehaviour
{
    private TextMesh healthText;
    private int _currentValue;
    private int _initialValue;
    private float _heightBrickOffset;
    private BrickDestroyedSignal _brickDestroyedSignal;
    private Pool _pool;

    public int InitialValue
    {
        get { return _initialValue; }
        private set { }
    }

    [Inject]
    public void Construct(
        BrickManager.Settings settings,
        BrickDestroyedSignal brickDestroyedSignal,
        Pool pool)
    {
        _currentValue = Random.Range(settings.minValue, settings.maxValue + 1);
        _initialValue = _currentValue;
        _heightBrickOffset = settings.heightBrickOffset;
        _brickDestroyedSignal = brickDestroyedSignal;
        _pool = pool;
        healthText = GetComponentInChildren<TextMesh>();
        healthText.text = _currentValue.ToString();
    }

    public void MoveDown()
    {
        var pos = transform.localPosition;
        pos.y -= _heightBrickOffset;
        transform.localPosition = pos;
    }

    public void GetDamage(int damage)
    {
        var newValue = _currentValue - damage;
        _currentValue = (newValue >= 0 ? newValue : 0);
        healthText.text = _currentValue.ToString();
        if(newValue <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
        _brickDestroyedSignal.Fire(this);
        _pool.Despawn(this);
    }

    public class Pool : MonoMemoryPool<Vector2, int, Brick>
    {
        protected override void Reinitialize(Vector2 position, int currentValue, Brick brick)
        {
            brick.transform.localPosition = position;
            brick._currentValue = currentValue;
            brick._initialValue = brick._currentValue;
            brick.healthText.text = brick._currentValue.ToString();
        }
    }

}
