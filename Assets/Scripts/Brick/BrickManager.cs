﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using Random = UnityEngine.Random;
using System.Linq;

public class BrickManager : IInitializable
{
    private List<Brick> _bricks = new List<Brick>();
    readonly private Settings _settings;
    readonly private Brick.Pool _brickPool;
    readonly private RowsMovingFinishedSignal _rowsMovingFinishedSignal;
    readonly private BallShotFinishedSignal _ballShotFinishedSignal;
    readonly private GameFinishedSignal _gameFinishedSignal;


    private float LosingPosition //=> _settings.initialPosition.y - (_settings.columnSize - 1) * _settings.heightBrickOffset;
    {
        get { return _settings.initialPosition.y - (_settings.columnSize - 1) * _settings.heightBrickOffset; }
    }

    public BrickManager(
        Settings settings,
        Brick.Pool brickPool,
        BallShotFinishedSignal ballShotFinishedSignal,
        RowsMovingFinishedSignal rowsMovingFinishedSignal,
        BrickDestroyedSignal brickDestroyedSignal,
        GameFinishedSignal gameFinishedSignal)
    {
        _settings = settings;
        _brickPool = brickPool;
        _rowsMovingFinishedSignal = rowsMovingFinishedSignal;
        _ballShotFinishedSignal = ballShotFinishedSignal;
        _gameFinishedSignal = gameFinishedSignal;
        ballShotFinishedSignal += GenerateNextRow;
        brickDestroyedSignal += OnBrickDestroy;
    }

    public void MoveBricks()
    {

        foreach (var brick in _bricks)
        {
            brick.MoveDown();
        }
        if (_bricks.Any(x => x.transform.position.y < LosingPosition))
        {
            _gameFinishedSignal.Fire(false);
        }
        else
        {
            _rowsMovingFinishedSignal.Fire();
        }
    }
    public void OnBrickDestroy(Brick brick)
    {
        _bricks.Remove(brick);
    }
    public void GenerateNextRow()
    {
        var amount = Random.Range(_settings.minBrickAmountInRow, _settings.maxBrickAmountInRow);
        int prevPlace = -1;
        for(int i = 0; i < amount; i++)
        {
            int newPlace = Random.Range(prevPlace + 1, _settings.rowSize - amount + i + 1);
            prevPlace = newPlace;
            var value = Random.Range(_settings.minValue, _settings.maxValue + 1);
            var brick = _brickPool.Spawn(new Vector2(newPlace * _settings.widthBrickOffset + _settings.initialPosition.x, _settings.initialPosition.y), value);
            _bricks.Add(brick);
        }
        MoveBricks();
    }

    public void Initialize()
    {
        _ballShotFinishedSignal.Fire();
    }

    [Serializable]
    public class Settings
    {
        public int minValue;
        public int maxValue;
        public int minBrickAmountInRow;
        public int maxBrickAmountInRow;
        public Vector2 initialPosition;
        public float widthBrickOffset;
        public float heightBrickOffset;
        public int rowSize;
        public int columnSize;
    }
}
