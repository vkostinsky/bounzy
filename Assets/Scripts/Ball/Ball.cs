﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class Ball : MonoBehaviour {

    Rigidbody2D _rigidBody2D;
    BallDestroyedSignal _ballDestroyedSignal;
    private int _damage;
    private Pool _pool;

    [Inject]
    public void Construct(BallManager.Settings settings,
        BallShotFinishedSignal ballShotFinishedSignal,
        BallDestroyedSignal ballDestroyedSignal,
        Pool pool)
    {
        _ballDestroyedSignal = ballDestroyedSignal;
        _pool = pool;
        _damage = settings.damage;
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }
 
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Finish"))
        {
            Die();
        }
        if(collision.gameObject.CompareTag("Brick"))
        {
            var brick = collision.gameObject.GetComponent<Brick>();
            if (brick != null)
            {
                brick.GetDamage(_damage);
            }
        }

    }
    public void SetVelocity(Vector2 direction)
    {
        _rigidBody2D.velocity = direction;
    }
    public void Die()
    {
        _ballDestroyedSignal.Fire();
        _pool.Despawn(this);
    }

    //public class Factory : Factory<Ball>
    //{
    //}

    public class Pool : MonoMemoryPool<Vector2, Vector2, Ball>
    {
        protected override void Reinitialize(Vector2 shootingPosition, Vector2 velocity, Ball ball)
        {
            ball.transform.localPosition = shootingPosition;
            ball.SetVelocity(velocity);
        }
    }
}
