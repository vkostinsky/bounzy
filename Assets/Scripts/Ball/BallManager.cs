﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class BallManager
{
    private int _ballsCount;
    readonly private Settings _settings;
    readonly private Ball.Pool _ballPool;
    readonly private Vector2 _shootingPosition;
    readonly private BallShotFinishedSignal _ballShotFinishedSignal;

    public BallManager(
            Settings settings,
            Ball.Pool ballPool,
            BallShotSignal ballShotSignal,
            ArrowManager.Settings shootingPositionSettings,
            BallDestroyedSignal ballDestroyedSignal,
            BallShotFinishedSignal ballShotFinishedSignal)
    {
        _settings = settings;
        _ballPool = ballPool;
        _shootingPosition = shootingPositionSettings.shootingPos;
        ballShotSignal += OnShot;
        ballDestroyedSignal += OnBallDestroyed;
        _ballShotFinishedSignal = ballShotFinishedSignal;
        _ballsCount = 0;
    }
    public void OnBallDestroyed()
    {
        _ballsCount--;
        if(_ballsCount<=0)
        {
            _ballShotFinishedSignal.Fire();
        }
    }

    public void OnShot(Vector2 direction)
    {
        var directionVector = direction - _shootingPosition;
        _ballPool.Spawn(_shootingPosition, directionVector.normalized * _settings.speed);
        _ballsCount++;
    }

    [Serializable]
    public class Settings
    {
        public float speed;
        public int damage;
        public int spawnCount;
        
        public float spawnDelay;
    }
}
